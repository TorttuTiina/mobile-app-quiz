import { Component, OnInit } from '@angular/core';
import { MovieQuestion } from '../../moviequestion';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  //Member variables
  questions: MovieQuestion[] = [];
  activeQuestion: MovieQuestion;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number;
  optionCounter: number;
  startTime: Date;
  endTime: Date;
  duration: number;

  constructor(public router: Router) {}

  ngOnInit() {
    fetch('../../assets/data/quiz.json').then(res => res.json())
    .then(json => {
      this.questions = json;
      this.setQuestion();
    });
    this.questionCounter = 0;
    //alert(this.activeQuestion); tällä voi testata toimiiko funktio
    this.startTime = new Date();
  } //end of ngOnInit

  setQuestion() {
    if (this.questionCounter === this.questions.length) {
      this.endTime = new Date();
      this.duration = this.endTime.getTime() - this.startTime.getTime();
      this.router.navigateByUrl('result/' + this.duration);
      this.ngOnInit();
    } else {
    this.optionCounter = 0;
    this.feedback = '';
    this.isCorrect = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
    }
}

  checkOption(option: number, activeQuestion: MovieQuestion) {
    this.optionCounter++;
    if (this.optionCounter > activeQuestion.options.length) {
      this.setQuestion();
    }
    if (option === activeQuestion.correctOption) {
    this.isCorrect = true;
    this.feedback = activeQuestion.options[option] + 
    ' on oikein! Näpäytä kuvaketta kerran!';
  } else {
    this.isCorrect = false;
    this.feedback = 'Hupsista! Arvauksia jäljellä: ' +
     (this.activeQuestion.options.length - this.optionCounter);
  } 
}
} //end of homepage
